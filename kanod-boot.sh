#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


KANODCONFIG=${KANODCONFIG:-${HOME}/.kanod}
ip=
user=
password=
url=
noproxy=0
verbose=1
vendor="dell"

# Failure message
function fail() {
  echo "$1" >&2
  if [ -n "${TMPDIR}" ]; then
    rm -rf "${TMPDIR}"
  fi
  exit 1
}

# debug message
function verbose() {
  if [ "$verbose" == "1" ]; then
    echo "$1" >&2
  fi
}

while getopts "a:c:hi:t:np:qu:?" opt; do
    case "$opt" in
    h|\?)
        echo "Usage: kanod_boot.sh [options]"
        echo ""
        echo "Options:"
        echo "    -a ip IP address of BMC"
        echo "    -c alternative configuration folder if used"
        echo "    -i url iso image URL"
        echo "    -t machine vendor (dell|hp)"
        echo "    -p BMC password"
        echo "    -u BMC user"
        echo "    -n no_proxy mode"
        echo "    -q quiet mode"
        exit 0
        ;;
    c)
        KANODCONFIG=${OPTARG}
        ;;
    a)
        ip=${OPTARG}
        ;;
    i)
        url=${OPTARG}
        ;;
    t)
        vendor=${OPTARG}
        ;;
    n)
        noproxy=1
        ;;
    p)
        password=${OPTARG}
        ;;
    q)
        verbose=0
        ;;
    u)
        user=${OPTARG}
        ;;
    esac
done

if [ -f "${KANODCONFIG}/config" ]; then
  set -a
  # shellcheck disable=SC1090,SC1091
  source "${KANODCONFIG}/config"
  set +a
fi

ISO=${ISO:-${MGT_NAME}.iso}
ip=${ip:-${BMC_IP}}
url=${url:-http://${NEXUS}/images/${ISO}}
user=${user:-$REDFISH_USER}
password=${password:-$REDFISH_PASSWORD}

tmpdir=$(mktemp -d)

if [ "$noproxy" == 1 ]; then
    unset http_proxy
    unset https_proxy
    CURL="curl --noproxy $ip"
else
    CURL="curl"
fi

# All the redfish commands in use.
if [ "${vendor}" = "dell" ]; then
    SystemState='Systems/System.Embedded.1/'
    ResetCommand='Systems/System.Embedded.1/Actions/ComputerSystem.Reset'
    GetCD='Managers/iDRAC.Embedded.1/VirtualMedia/CD'
    InsertCDCommand='Managers/iDRAC.Embedded.1/VirtualMedia/CD/Actions/VirtualMedia.InsertMedia'
    EjectCDCommand='Managers/iDRAC.Embedded.1/VirtualMedia/CD/Actions/VirtualMedia.EjectMedia'
    SetNextBootCommand='Managers/iDRAC.Embedded.1/Actions/Oem/EID_674_Manager.ImportSystemConfiguration'
    StatusOk=204
elif [ "${vendor}" = "hp" ]; then
    SystemState='Systems/1/'
    ResetCommand='Systems/1/Actions/ComputerSystem.Reset'
    GetCD='Managers/1/VirtualMedia/2'
    InsertCDCommand='Managers/1/VirtualMedia/2/Actions/VirtualMedia.InsertMedia/'
    EjectCDCommand='Managers/1/VirtualMedia/2/Actions/VirtualMedia.EjectMedia/'
    StatusOk=200
else
    echo "unknown vendor : ${vendor}"
    exit 1
fi

# Generic function to perform Redfish request.
#   request VERB SUBURL [DATA]
# VERB is a HTTP verb
# SUBURL is the part of the URL after redfish/v1
# DATA is an optional json payload (use curl @file syntax if needed)
# Status code is on stdout and result is in $tmpdir/data.json
function request() {
    query="https://${ip}/redfish/v1/$2"
    header='Content-Type: application/json'
    if [ -z "$3" ]; then
        ${CURL} -o "${tmpdir}/data.json" -w '%{http_code}' \
                --insecure -X "$1" "$query" -u "${user}:${password}" \
                -H "$header" 2>/dev/null
    else
        ${CURL} -o "${tmpdir}/data.json" -w "%{http_code}" \
                --insecure -X "$1" "$query" -u "${user}:${password}" \
                -H "$header" --data "$3" 2>/dev/null
    fi
    # shellcheck disable=SC2181
    if [ "$?" != 0 ]; then
        echo "Redfish query failed: $2" >&2
        exit 1
    fi
}

# Force turning off the server
function force_off() {
    verbose 'Forced Shutdown'
    status=$(request POST $ResetCommand '{"ResetType": "ForceOff"}')
    if [ "$status" != ${StatusOk} ]; then
        echo 'Forced shutdown failed' >&2
        return 1
    fi
}

# Shutdown the server gracefully
function turn_off() {
    verbose 'Shutdown'
    status=$(request POST $ResetCommand '{"ResetType": "GracefulShutdown"}')
    if [ "$status" != ${StatusOk} ]; then
        echo 'Shutdown failed' >&2
        return 1
    fi
}

# Restart the server
function restart() {
    verbose 'Restart'
    status=$(request POST $ResetCommand '{"ResetType": "On"}')
    if [ "$status" != ${StatusOk} ]; then
        echo 'Restart failed' >&2
        return 1
    fi
}

# Get the power state of the server
function power_state {
    status=$(request GET ${SystemState})
    if [ "$status" == 200 ]; then
        jq -r .PowerState "$tmpdir/data.json"
    else
        echo 'unknown'
    fi
}

function state {
    status=$(request GET ${SystemState})
    if [ "$status" == 200 ]; then
        jq -r .Status.State "$tmpdir/data.json"
    else
        echo 'unknown'
    fi
}


# Check the status and wait until server is off
# shellcheck disable=SC2120
function wait_for_off {
    max=${1:-20}
    count=0
    while true; do
        if [ "$(power_state)" == "Off" ]; then
            break
        else
            if [ "$verbose" == 1 ]; then
                echo -n '.'
            fi
        fi
        (( count+=1 ))
        if [ "$count" == "$max" ]; then
            return 1
        fi
        sleep 2
    done
}

function wait_for_on {
    while true; do
        status=$(request GET "$SystemState" )
        if [ "$(power_state)" == "On" ]; then
            break
        else
            if [ "$verbose" == 1 ]; then
                echo -n '.'
            fi
        fi
        sleep 2
    done
}

function wait_for_ready {
    while true; do
        status=$(request GET "$SystemState" )
        if [ "$(state)" == "Enabled" ]; then
            break
        elif [ "$(state)" == "Ready" ]; then
            break
        else
            if [ "$verbose" == 1 ]; then
                echo -n '.'
            fi
        fi
        sleep 2
    done
}

# Shutdown system.
#
# Tries to make a graceful shutdown, force it if needed.
function shutdown {
    status=$(request GET "$SystemState" )
    if [ "$(power_state)" == "On" ]; then
            verbose "Halt system"
            if turn_off; then
                wait_for_off 20 || { force_off && wait_for_off 20; } || return 1
            else
                return 1
            fi
	    # Needs time to settle before CD ready. How much ?
            sleep 15
    else
        verbose 'System already down'
    fi
}

# Weird set_next_boot command
#
# TODO: We do not try to get the task status the Dell way.
function set_next_boot {
    verbose 'Set next boot to CD'
    payload='{"ShareParameters": {"Target": "ALL"}, "ImportBuffer": "<SystemConfiguration><Component FQDD=\"iDRAC.Embedded.1\"><Attribute Name=\"ServerBoot.1#BootOnce\">Enabled</Attribute><Attribute Name=\"ServerBoot.1#FirstBootDevice\">VCD-DVD</Attribute></Component></SystemConfiguration>"}'
    status=$(request POST "$SetNextBootCommand" "$payload")
    sleep 10
    cat "$tmpdir/data.json"
}

function set_next_boot_cd_hp {
    verbose 'Set next boot to CD'
    payload='{"Boot": {"BootSourceOverrideTarget": "Cd","BootSourceOverrideEnabled": "Once"}}'
    status=$(request PATCH "$SystemState" "$payload")
    sleep 10
}

function set_next_boot_hdd_hp {
    verbose 'Set next boot to HDD'
    payload='{"Boot": {"BootSourceOverrideTarget": "Hdd","BootSourceOverrideEnabled": "Once"}}'
    status=$(request PATCH "$SystemState" "$payload")
    sleep 10
}

# Get the status of virtual CD
function get_cd_status {
    status=$(request GET $GetCD)
    if [ "$status" != 200 ]; then
        verbose "unknown"
        return 1
    fi
    jq .Inserted "$tmpdir/data.json"
}

# Eject the CD
function eject_cd {
    status=$(request POST "$EjectCDCommand" '{}')
    if [ "$status" != 204 ]; then
        return 1
    fi
}

# Insert a cd
#
# insert_cd URL 
function insert_cd {
    payload="{\"Image\": \"$1\", \"Inserted\": true, \"WriteProtected\": true}"
    status=$(request POST "$InsertCDCommand" "$payload")
    if [ "$status" != ${StatusOk} ]; then
        echo 'Insert CD failed' >& 2
        cat "$tmpdir/data.json"
        return 1
    else
        verbose 'CD inserted'
    fi
}


# Eject CD if necessary and check it is ejected
# shellcheck disable=SC2120
function wait_for_cd_ejected {
    max=${1:-20}
    count=0
    if [ "$(get_cd_status)" == "true" ]; then
        verbose 'Eject old CD'
        eject_cd
        while true; do
            if [ "$(get_cd_status)" == "false" ]; then
                break
            else
                if [ "$verbose" == 1 ]; then
                    echo -n '.'
                fi
            fi
            (( count+=1 ))
            if [ "$count" == "$max" ]; then
                return 1
            fi
            sleep 2
        done
    fi
    sleep 2
}

if [ "${vendor}" = "dell" ]; then
    shutdown && wait_for_cd_ejected 20 && insert_cd "$url" && set_next_boot && restart
elif [ "${vendor}" = "hp" ]; then
    shutdown && wait_for_cd_ejected 20 && insert_cd "$url" && set_next_boot_cd_hp && restart && wait_for_on && wait_for_ready && set_next_boot_hdd_hp
else
    echo "unknown vendor : ${vendor}"
    exit 1
fi


rm -rf "$tmpdir"
