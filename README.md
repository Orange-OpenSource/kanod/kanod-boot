# kanod-boot - Scripts for launching images

The kanod-boot module provides two utilities to start an Kanod OS image
with a suitable configuration:

* `kanod-vm` will launch a virtual machine
* `kanod-prepare` will buld a iso image that can be used as a virtual-media
  to install the OS image and its configuration on the specified disk of a
  server.

Documentation: https://orange-opensource.gitlab.io/kanod/reference/launch/index.html