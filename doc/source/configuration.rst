Manifests Fields
================

.. jsonschema:: common_schema.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:

.. jsonschema:: vm_schema.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:

.. jsonschema:: iso_schema.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:
