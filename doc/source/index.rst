=======================
Kanod machine instances
=======================

.. toctree::
    :maxdepth: 2

    launching-machines
    kanod-vm
    kanod-prepare
    configuration

