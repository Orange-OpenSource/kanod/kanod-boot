Overview
========

To operate a Kanod deployment, several machines are necessary. These instances can be either physical servers or virtual
machines.

Kanod tries to blur the distinction between support machines
launched manually, fully managed Kubernetes nodes and machines
managed as Metal3 baremetal hosts. All of them stem from qcow2
OS images that are specialized with a simplified ``cloud-init``
file where all the moving part is comprised in a YAML descriptor.

The utilities in the ``kanod-boot`` project are used to launch
manually such an image.

A machine is described by a folder. This folder contains the
following elements:

* a configuration file containing the parameters of the
  instance. It is a yaml file named ``config.yaml``.
* an optional initial OS image ``img.qcow2``. If it not
  given, it must be hosted on the Nexus and specified
  in the configuration file.
* a pair of public (``key.pub``) and private key (``key``)
  for SSH connection. If they are not supplied, they are generated.

When a virtual machine is launched. The following files are
generated:

* a disk instance ``disk.qcow2`` that will be the main disk of
  the instance.
* a ``cloud-init`` boot disk ``cidata.iso`` that contains the
  rendered configuration.

Legacy mode
-----------

There is a legacy mode for machines that are not using declarative
configuration. If a file ``user-data`` is present, it is interpreted
as a complete cloud-init configuration file and is used instead of the
one synthesized by kanod-boot utilities.

In this mode, shell variables appearing in the ``user-data`` files can
be substituted if they are defined in the ``variables`` entry of the
``config.yaml`` file.