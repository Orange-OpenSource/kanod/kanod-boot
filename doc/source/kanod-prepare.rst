``kanod-prepare`` - Creating bootable iso
=========================================
``kanod-prepare`` generates iso files that can be booted on real server
as virtual media. During the first boot the file is written to disk in
the disk identified by ``boot.disk``. This boot must be done in **legacy
BIOS mode**. This does not mean that the burned OS cannot be started in
UEFI but the current version uses isolinux in a way that is not compatible 
with UEFI.

``kanod-prepare`` requires a burner OS from project ``burner-builder``.
It is usually present on the Nexus server. If it is not yet deployed, it is
possible to build the image independently and copy the kernel and ram file
system in the configuration folder of the system.

In ``burner-builder`` run

.. code-block:: shell

   burner-build.sh -o burner

Then copy ``burner.kernel`` and ``burner.initramfs`` in the same folder as
``config.yaml``.

The layout of the configuration folder is the following:

.. code-block::

   server/
   ├── burner.initramfs   # optional (taken on nexus otherwise)
   ├── burner.kernel      # optional (taken on nexus otherwise)
   ├── cidata.iso         # generated
   ├── config.yaml        # mandatory
   ├── disk.qcow2         # generated
   ├── image.iso          # generated : the image to burn
   ├── img.qcow2          # optional (taken on nexus otherwise)
   ├── key                # optional - generated otherwise
   ├── key.pub            # optional - generated otherwise
   └── user-data          # optional - legacy mode only


Options
-------
.. option:: -c <config>, --config <config>

   Specifies the path to the configuration folder

.. option:: -h, --help

   Display the help.

.. option:: -i <img>

   Specifies the path to the OS image to use

.. option:: -o <file>

   Specify the name of the iso file e generated. Default is
   ``image.iso``

.. option:: -q, --quiet

   Quiet mode without informative output.
