``kanod-vm`` - Launching Virtual Machines
=========================================

Kanod-vm launches a virtual machine on the host using the declarative
description provided in a folder. The simplest call is:

.. code-block:: shell

   kanod-vm -c server-conf

where the layout of the configuration folder ``server-conf`` is the following:

.. code-block::

   server-conf/
   ├── cidata.iso         # generated
   ├── config.yaml        # mandatory
   ├── disk.qcow2         # generated - the current disk
   ├── img.qcow2          # optional (taken on nexus otherwise)
   ├── key                # optional - generated otherwise
   ├── key.pub            # optional - generated otherwise
   └── user-data          # optional - legacy mode only


Options
-------
.. option:: -c <config>, --config <config>

   Specifies the path to the configuration folder

.. option:: -h, --help

   Display the help.

.. option:: -i <img>

   Specifies the path to the OS image to use

.. option:: -q, --quiet

   Quiet mode without informative output.

.. option:: -v, --validate

   Force the validation of the configuration file.

.. option:: -V <schema>

   Specify the schema to use for the validation. If the
   image is retrieved from a Nexus repository, the schema
   should be a companion artefact of the image.

