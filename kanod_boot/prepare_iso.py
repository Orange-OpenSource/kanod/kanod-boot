#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import argparse
from os import path

from . import common
from . import config_util
from . import validator


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config",
        help="Specify an alternate config folder"
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="Quiet mode"
    )
    parser.add_argument(
        "-o", "--output", help="output filename for the iso"
    )
    parser.add_argument(
        "-u", "--upload", action="store_true", help="Upload the image"
    )
    parser.add_argument(
        "-i", "--img", help="Image name to use"
    )
    parser.add_argument(
        "-v", "--validate", action="store_true",
        help="Validate the configuration"
    )
    parser.add_argument(
        "-V", "--validation-file",
        help="Validate the configuration"
    )
    args = parser.parse_args()
    config_util.setup_yaml_formatter()
    config_util.check_command(
        ['genisoimage', 'qemu-img', 'ssh-keygen'])
    kanodconfig, config = common.find_config(args)
    common.vault_prepare(config)
    maven_context = config_util.maven_context(config, kanodconfig)
    if args.validate:
        validator.validate(
            kanodconfig, config, maven_context,
            file=args.validation_file, aux=validator.ISO_SCHEMA)
    common.get_keys(kanodconfig, config)
    config.setdefault('name', 'test')
    output = (
        args.output if args.output is not None
        else path.join(kanodconfig, 'image.iso'))
    common.make_cloud_iso(
        kanodconfig, config, maven_context,
        burner=True, img=args.img, output=output)
