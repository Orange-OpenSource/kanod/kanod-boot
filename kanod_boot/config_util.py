#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from os import path
import re
import shutil
import sys
import yaml
from yaml.resolver import Resolver

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader  # type: ignore[misc]


def load(filename):
    """Load a yaml file from a file descriptor

    :param filename: yaml file
    :return: object corresponding to yaml file
    """
    try:
        with open(filename, 'r') as fd:
            result = yaml.load(fd, Loader=Loader)
    except yaml.parser.ParserError as e:
        sys.exit(f'Yaml loading exited with error: {e}')
    except FileNotFoundError as e:
        sys.exit(f'Yaml file not found: {e}')
    except OSError as e:
        sys.exit(f'Yaml file cannot be read: {e}')
    return result


class literal(str):
    '''long string class

    A class to wrap long multi-line strings that should be output in yaml
    using the pipe.
    '''

    pass


def literal_presenter(dumper, data):
    '''Function to output literal in pyyaml'''
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')


def setup_yaml_formatter():
    '''Add a formatter for class literal used for long strings'''
    yaml.add_representer(literal, literal_presenter)

def compliant_yaml_1_2():
    '''Disable parsing of sexagesimal ints.

    Required for mac address parsing and removed in Yaml 1.2
    solution described at: https://stackoverflow.com/a/31007425
    '''
    for ch in list(u'-+0123456789'):
        del Resolver.yaml_implicit_resolvers[ch]
    Resolver.add_implicit_resolver(
        u'tag:yaml.org,2002:int',
        re.compile(u'''^(?:[-+]?0b[0-1_]+
        |[-+]?0o?[0-7_]+
        |[-+]?(?:0|[1-9][0-9_]*)
        |[-+]?0x[0-9a-fA-F_]+)$''', re.X),
        list(u'-+0123456789'))

def dump(content, filename=None, header=None):
    """Dumps a yaml file

    with an optional header either in a file or on stdout.
    """
    if filename is None:
        if header is not None:
            return header + yaml.dump(content)
        return yaml.dump(content)
    else:
        try:
            with open(filename, 'w') as fd:
                if header is not None:
                    fd.write(header)
                yaml.dump(content, stream=fd)
        except OSError as e:
            sys.exit(f'Cannot create Yaml file {filename}: {e}')


def check_command(commands):
    """Check that all the commands of a list are available

    Otherwise brutally fails.
    """
    unknown = []
    for command in commands:
        if shutil.which(command) is None:
            unknown.append(command)
    if len(unknown) > 0:
        unknown_text = ', '.join(unknown)
        sys.exit(f'You must install: {unknown_text}')


def maven_context(config, kanodconfig):
    """configuration for requests on maven

    extract the maven URL and the optional ca certificate.
    The result is the pair url validate, where validate can be given as value
    to requests optional argument validate. It is True if there is no 
    certificate or the path to a temporary file otherwise."""

    nexus = config.get('nexus', {})
    maven = nexus.get('maven', None)
    if maven is None:
        return None
    certificate = nexus.get('certificate', None)
    if certificate is None:
        return (maven, True)
    if certificate == '@vault:ca':
        vault = config.get('vault', None)
        certificate = vault.get('ca', None)
    crt = path.join(kanodconfig, 'nexus.pem')
    with open(crt, 'w') as fd:
        fd.write(certificate)
    return (maven, crt)
