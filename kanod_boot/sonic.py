#  Copyright (C) 2022-2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import json
import logging
import os
from os import path
import subprocess
import sys
import yaml

from . import config_util

logger = logging.getLogger(__name__)



def generate_config(
    file,
    name, mac, interfaces,
    vlans=[],
    address=None,
    gateway=None,
    bgp_asn=65100,
    loopback='10.1.0.1',
):
    cfg = {
        "DEVICE_METADATA": {
            "localhost": {
                "hwsku": "montara",
                "platform": "x86_64-kvm_x86_64-r0",
                "mac": mac,
                "hostname": name,
                "type": "LeafRouter",
                "bgp_asn": bgp_asn
            }
        },
        "PORT": {
            f"Ethernet{i}": {
                "lanes": f"{i*4},{1+i*4},{2+i*4},{3+i*4}",
                "alias": "fortyGigE0/0",
                "index": f"{i}",
                "speed": "40000",
                "admin_status": "up",
                "mtu": "1500"
            }
            for i in range(len(interfaces))
        },
        "FLEX_COUNTER_TABLE": {
            "ACL": {
                "FLEX_COUNTER_STATUS": "disable",
                "FLEX_COUNTER_DELAY_STATUS": "true",
                "POLL_INTERVAL": "10000"
            }
        },
        "LOOPBACK_INTERFACE": {
            f"Loopback0|{loopback}/32": {}
        },
        "BGP_NEIGHBOR": {},
        "DEVICE_NEIGHBOR": {},
        "MGMT_PORT": {
            "eth0": {
                "alias": "eth0",
                "admin_status": "up"
            }
        },
        "INTERFACE": {
            f"Ethernet{i}|{itf['address']}": {}
            for (i, itf) in enumerate(interfaces)
            if 'address' in itf
        },
        "VLAN": {
            f"Vlan{vlan['id']}" : {"vlanid": str(vlan["id"])}
            for vlan in vlans
        },
        "VLAN_INTERFACE": {
            **{
                f"Vlan{vlan['id']}": {"proxy_arp": "enabled"}
                for vlan in vlans
            },
            **{
                f"Vlan{vlan['id']}|{vlan['address']}": {}
                for vlan in vlans
                if 'address' in vlan
            }
        },
        "VLAN_MEMBER": {
            f"Vlan{vlan['id']}|{member['itf']}": {
                "tagging_mode": ("tagged" if member.get('tagged', True) else "untagged")
            }
            for vlan in vlans
            for member in vlan.get('members', [])
        }
    }
    if address is not None:
        cfg_mgt = {}
        if gateway is not None:
            cfg_mgt["gwaddr"] = gateway
        cfg["MGMT_INTERFACE"] = {
            f"eth0|{address}": cfg_mgt
        }

    with open(file, 'w', encoding='utf-8') as fp:
        json.dump(cfg, fp, indent=4)
        fp.write('\n')


def modify_image(conf, dest_folder, image):
    print(conf)
    config_file = path.join(dest_folder, 'config_db.json')
    sonic = conf.get('sonic', {})
    vm = conf.get('vm', {})
    name = conf.get('name', 'sonic')
    itfs = vm.get('networks', [])
    if len(itfs) < 1:
        raise Exception('First interface (management) is mandatory')
    itfs = itfs[1:]
    mac = sonic.get('mac', '52:54:00:05:02:03')
    asn = sonic.get('asn', 65100)
    mgmt = sonic.get('management', {})
    address = mgmt.get('address', None)
    gateway = mgmt.get('gateway', None)
    loopback = mgmt.get('loopback', '10.1.0.1')
    vlans = sonic.get('vlans', [])
    generate_config(
        config_file, name, mac, itfs, vlans, address, gateway,
        bgp_asn=asn, loopback=loopback)
    # Use a default sku montara as it has a simpler port mapping than
    # Force6000
    default_sku = path.join(dest_folder, 'default_sku')
    with open(default_sku, 'w', encoding='utf-8') as fp:
        fp.write('montara t1\n')
    ssh = conf.get('admin', {}).get('keys',[])
    pubkeyfile = path.join(dest_folder, 'key.pub')
    if path.exists(pubkeyfile):
        with open(pubkeyfile, 'r') as fd:
            mgt_key = fd.read()
            ssh.append(mgt_key)
    if len(ssh) > 0:
        ssh_folder = path.join(dest_folder, '.ssh')
        try:
            os.mkdir(ssh_folder)
        except FileExistsError:
            pass
        key_files = path.join(ssh_folder, 'authorized_keys')
        with open(key_files, 'w', encoding='utf-8') as fd:
            for line in ssh:
                fd.write(line)
                fd.write('\n')
    else:
        ssh_folder = None

    guestfish_command = [
        'sudo', 'guestfish', '--rw', '-a', image, '-m', '/dev/sda3'
    ]
    # Find path to image folder with right revision number.
    output = subprocess.check_output(
        [*guestfish_command, 'ls', '/'], encoding='utf-8')
    base = next(
        line for line in output.split('\n') if line.startswith('image-')
    )

    def inject(dest_folder, *files, prefix='rw'):
        img_path = f'/{base}/{prefix}{dest_folder}'
        command = [*guestfish_command, 'mkdir-p', img_path]
        subprocess.run(command, check=True)
        command = [*guestfish_command, 'copy-in', *files, img_path]
        subprocess.run(command, check=True)

    # Select the right "default" sku. Changing the config_db is not enough
    inject('/usr/share/sonic/device/x86_64-kvm_x86_64-r0', default_sku)
    # Copy configuration and restapi credentials in folder.
    inject('/etc/sonic', config_file)
    if ssh_folder is not None:
        inject('/home/admin', ssh_folder)

def main():
    config_util.setup_yaml_formatter()
    config_util.check_command(['guestfish'])
    dest_folder = sys.argv[1]
    image = path.join(dest_folder, 'disk.qcow2')
    config_file = path.join(dest_folder, 'config.yaml')
    with open(config_file, 'r', encoding='utf-8') as fd:
        config = yaml.safe_load(fd)
    modify_image(config, dest_folder, image)
