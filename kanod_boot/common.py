#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import base64
import getpass
import os
from os import environ, path
import requests
import shutil
import subprocess
import sys
import tempfile
import xml.etree.ElementTree as ET

from . import config_util
from . import dictsubst


def download_file(url, verify, target):
    with requests.get(url, verify=verify, stream=True) as r:
        r.raise_for_status()
        with open(target, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

def get_maven_url(maven_context, artefact, version, format, extension=None):
    '''Find the URL of an image using maven manifests if necessary.'''
    if maven_context is None:
        sys.exit(
            'Cannot find the image repository (nexus.maven not defined)')
    (maven, verify) = maven_context
    download_base = f'{maven}/repository/kanod/kanod'
    if version == 'latest' or version == 'release' :
        metadata_url = f'{download_base}/{artefact}/maven-metadata.xml'
        req = requests.get(
            metadata_url,
            verify=verify)
        if req.status_code != 200:
            raise Exception('Cannot find metadata for best image')
        root = ET.fromstring(req.content.decode('utf-8'))
        path = f'versioning/{version}'
        print(path + " "  + str(root))
        version = root.find(path).text
    suffix = '' if extension is None else f'-{extension}'
    return (
        f'{download_base}/{artefact}/{version}/'
        f'{artefact}-{version}{suffix}.{format}')


def define_disk(kanodconfig, config, maven_context, target_disk, img=None):
    img_disk = img if img is not None else path.join(kanodconfig, 'img.qcow2')
    if path.exists(target_disk):
        os.remove(target_disk)
    if path.exists(img_disk):
        print(f"Using img from: {img_disk}")
        shutil.copyfile(img_disk, target_disk)
        return True
    else:
        img = config.get('image', None)
        format = config.get('imageFormat', 'qcow2')
        if img is None:
            os_name = config.get('os', 'focal')
            if os_name == 'none':
                return False
            kubernetes = config.get('kubernetes', {})
            k8s = kubernetes.get('version', 'v1.18.8')
            img = f'{os_name}-{k8s}'
        version = config.get('version', 'release')
        url =  get_maven_url(maven_context, img, version, format)
        (_, verify) = maven_context
        try:
            print(f"Using img from: {url}")
            download_file(url, verify, target_disk)
            return True
        except Exception:
            raise Exception(f'Did not find relevant image at {url}')


SYSLINUX_URL = (
    'https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/'
    'syslinux-6.03.tar.xz'
)


def add_iso_linux(tempdir):
    syslinux_archive = path.join(tempdir, 'syslinux.tar.xz')
    isolinux_dir = path.join(tempdir, 'isolinux')
    print(f"Downloading  {SYSLINUX_URL}")
    try:
        download_file(SYSLINUX_URL, True, syslinux_archive)
    except Exception:
        raise Exception(f'Failed to downoload syslinux')

    os.mkdir(isolinux_dir)
    for file in ['isolinux.bin', 'ldlinux.c32']:
        with open(path.join(isolinux_dir, file), 'w') as fd:
            command = [
                'tar', '--no-anchored', '-xJOvf', syslinux_archive, file]
            if subprocess.run(command, stdout=fd).returncode != 0:
                raise Exception('Extraction from syslinux failed')
    os.remove(syslinux_archive)
    with open(path.join(isolinux_dir, 'isolinux.cfg'), 'w') as fd:
        fd.writelines([
            'DEFAULT install\n'
            'LABEL install\n'
            '    menu label "Install image"\n'
            '    kernel /vmlinuz\n'
            '    append initrd=/initrd boot_method=vmedia --\n'
            'TIMEOUT 5\n'
            'PROMPT 0\n'
        ])


def add_burner(maven_context, tempdir, kanodconfig):
    '''Adds a burner to the iso image

    :param maven_context: url and pointer to CA file for Maven
    :param tempdir: temporary directory containing the ISO content
    :param kanodconfig: Kanod configuration file describing the ISO.
    '''
    burner_parts = [('vmlinuz', 'kernel'), ('initrd', 'initramfs')]
    for (file, extension) in burner_parts:
        component = path.join(kanodconfig, f'burner.{extension}')
        if path.exists(component):
            shutil.copy(component, path.join(tempdir, file))
        else:
            if maven_context is None:
                raise Exception(
                    f"Burner component {extension} not found at {component}")
            (_, verify) = maven_context
            url = get_maven_url(maven_context, 'burner', 'release', extension)
            try:
                download_file(url, verify, f'{tempdir}/{file}')
            except Exception:
                raise Exception(f'Failed to find burner {extension} at {url}')
    os.chmod(path.join(tempdir, 'vmlinuz'), 0o755)


def legacy_config(config, kanodconfig, cidata):
    user_data_legacy = path.join(kanodconfig, 'user-data')
    user_data = path.join(cidata, 'user-data')
    with open(user_data_legacy, 'r', encoding='utf-8') as fd:
        user_data_content = fd.read()
        variables = config.get('variables', {})
        variables['MGT_KEY'] = config.get('admin', {}).get('keys', [])[-1]
        subst = dictsubst.dictsubst(variables)
        with open(user_data, 'w', encoding='utf-8') as fd2:
            fd2.write(subst.substitute(user_data_content))

    network_data_legacy = path.join(kanodconfig, 'network-data')
    if path.exists(network_data_legacy):
        network_data = path.join(cidata, 'network-config')
        shutil.copy(network_data_legacy, network_data)


def make_cloud_iso(
    kanodconfig, config, maven_context,
    burner=False, img=None, output=None
):
    cidata = tempfile.mkdtemp()
    # user-data generation
    user_data = path.join(cidata, 'user-data')
    if path.exists(path.join(kanodconfig, 'user-data')):
        legacy_config(config, kanodconfig, cidata)
    else:
        config_data = config_util.literal(config_util.dump(config))
        runcmd = config.get('runcmd', [])
        user_data_content = {
            "runcmd": [
                "kanod-runcmd"] + runcmd,
            "write_files": [
                {"path": "/etc/kanod-configure/configuration.yaml",
                 "content": config_data}]
        }

        bootspec = config.get('boot')
        if bootspec is not None:
            code = f'kanod-bootcmd <<EOF\n{config_util.dump(bootspec)}\nEOF\n'
            user_data_content["bootcmd"] = [config_util.literal(code)]

        mounts = config.get('mounts')
        if mounts is not None:
            user_data_content['mounts'] = mounts

        disk_setup = config.get('disk_setup', {})
        partitions = disk_setup.get('partitions')
        if partitions is not None:
            user_data_content['disk_setup'] = partitions
        filesystems = disk_setup.get('filesystems')
        if filesystems is not None:
            user_data_content['fs_setup'] = filesystems

        config_util.dump(
            user_data_content, user_data, header='#cloud-config\n')
    # meta-data generation
    meta_data = path.join(cidata, 'meta-data')
    name = config.get('name', 'test')
    format = config.get('imageFormat', 'qcow2')
    if burner:
        add_iso_linux(cidata)
        add_burner(maven_context, cidata, kanodconfig)
        target_disk = path.join(cidata, f'disk.{format}')
        define_disk(kanodconfig, config, maven_context, target_disk, img)
    with open(meta_data, 'w') as fd:
        fd.writelines([
            f'instance-id: {name}\n',
            f'local-hostname: {name}\n'
        ])
        if burner:
            boot = config.get('boot', {})
            disk = boot.get('disk', None)
            if disk is not None:
                fd.write(f'boot_disk: {disk}\n')
            debug = boot.get('debug', False)
            fd.write(f'debug_burner: {1 if debug else 0}\n')
            no_auto_reboot = boot.get('no_auto_reboot', False)
            fd.write(f'no_auto_reboot: {1 if no_auto_reboot else 0}\n')

    output = path.join(kanodconfig, 'cidata.iso') if output is None else output
    if path.exists(output):
        os.remove(output)

    command = [
        'genisoimage', '-input-charset', 'utf-8', '-output', output,
        '-volid', 'cidata', '-joliet', '-rock']
    if burner:
        command += [
            '-b', 'isolinux/isolinux.bin',
            '-c', 'isolinux/boot.cat', '-no-emul-boot',
            '-boot-load-size', '4', '-boot-info-table'
        ]
    command += ['-graft-points', f'.={cidata}']

    if subprocess.run(command).returncode != 0:
        raise Exception('generation of iso image failed.')
    shutil.rmtree(cidata)


def find_config(args):
    if args.config is not None:
        kanodconfig = args.config
    else:
        kanodconfig = os.environ.get(
            'KANODCONFIG',
            path.join(os.environ.get('HOME', ''), '.kanod'))
    if not path.isdir(kanodconfig):
        sys.exit(
            f'Cannot find the kanod configuration folder at {kanodconfig}')
    config = config_util.load(path.join(kanodconfig, 'config.yaml'))
    return kanodconfig, config


def vault_prepare(config):
    vault = config.get('vault', None)
    if vault is None:
        return
    print('Configuring for vault')
    cacert = vault.get('ca', None)
    if cacert is None:
        verify = True
    else:
        with tempfile.NamedTemporaryFile(
            mode='w', delete=False, encoding='utf-8'
        ) as fd:
            fd.write(cacert)
            fd.write('\n')
            verify = fd.name
    role = vault.get('role', None)
    url = vault.get('url', None)
    if role is None or url is None:
        raise Exception('Role and URL required for vault configuration')
    token_path = path.join(os.environ.get('HOME', ''), '.kanod_token')
    if path.exists(token_path):
        with open(token_path, 'r') as fd:
            token = fd.read()
        req = requests.get(
            f'{url}/v1/auth/token/lookup-self',
            headers={'X-Vault-Token': token},
            verify=verify)
        if req.status_code != 200:
            token = None
    else:
        token = None
    if token is None:
        print('Getting a token for vault')
        user = os.environ.get('KANOD_USER', None)
        if user is None:
            user = input('Vault login: ')
        password = os.environ.get('KANOD_PASSWORD', None)
        if password is None:
            password = getpass.getpass('Vault password:')
        req = requests.post(
            f'{url}/v1/auth/userpass/login/{user}',
            json={'password': password}, verify=verify
        )
        if req.status_code != 200:
            print(f'Error ({req.status_code}): {req.content.decode("utf-8")}')
            raise Exception('Cannot log as user')
        user_token = req.json().get('auth', {}).get('client_token', None)
        req = requests.post(
            f'{url}/v1/auth/approle/role/boot/secret-id',
            headers={'X-Vault-Token': user_token}, verify=verify
        )
        if req.status_code != 200:
            print(f'Error ({req.status_code}): {req.content.decode("utf-8")}')
        else:
            secret_id = req.json().get('data', {}).get('secret_id', None)
        if secret_id is None:
            raise Exception('Cannot get boot secret')
        req = requests.post(
            f'{url}/v1/auth/approle/login',
            json={'role_id': 'boot-role-id', 'secret_id': secret_id},
            verify=verify
        )
        if req.status_code != 200:
            print(f'Error ({req.status_code}): {req.content.decode("utf-8")}')
        else:
            token = req.json().get('auth', {}).get('client_token', None)
        if token is None:
            raise Exception('Vault: cannot continue without token')
        with open(
            os.open(token_path, os.O_CREAT | os.O_WRONLY, 0o600), 'w'
        ) as fd:
            fd.write(token)

    req = requests.post(
        f'{url}/v1/auth/approle/role/{role}/secret-id',
        headers={'X-Vault-Token': token}, verify=verify
    )
    if req.status_code != 200:
        print(f'Error ({req.status_code}): {req.content.decode("utf-8")}')
        raise Exception('Vault: cannot get token for role {role}')
    secret_id = req.json().get('data', {}).get('secret_id', None)
    vault['secret_id'] = secret_id
    if isinstance(verify, str):
        os.remove(verify)


def get_keys(kanodconfig, config):
    mgt_key = config.get('mgt_key')
    if mgt_key is not None:
        return mgt_key
    keyfile = path.join(kanodconfig, 'key')
    if not path.exists(keyfile):
        command = ['ssh-keygen', '-b', '2048', '-f', keyfile, '-N', '']
        if subprocess.run(command).returncode != 0:
            raise Exception('Key generation failed')
    pubkeyfile = path.join(kanodconfig, 'key.pub')
    if path.exists(pubkeyfile):
        with open(pubkeyfile, 'r') as fd:
            mgt_key = fd.read()
            admin = config.setdefault('admin', {})
            keys = admin.setdefault('keys', [])
            keys.append(mgt_key)
        return mgt_key
    else:
        return None
