#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import argparse
from os import path
import subprocess
import time

from . import common
from . import config_util
from . import interfaces
from . import validator


def cleanup_previous_vm(host):
    running_vm = subprocess.run(
        ['virsh', 'list', '--name', '--state-running'],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if host in running_vm.stdout.decode('utf-8').split('\n'):
        if subprocess.run(['virsh', 'destroy', host]).returncode != 0:
            print('Warning: destroying VM failed.')
    defined_vm = subprocess.run(
        ['virsh', 'list', '--name', '--all'],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if host in defined_vm.stdout.decode('utf-8').split('\n'):
        if subprocess.run(['virsh', 'undefine', '--nvram', host]).returncode != 0:
            print('Warning: undefining VM failed.')


def resize_disk(config, target_disk):
    vm = config.get('vm', {})
    disk_size = vm.get('disk_size', 12)
    command = ['qemu-img', 'resize', target_disk, f'{disk_size}G']
    if subprocess.run(command).returncode != 0:
        raise Exception('Failed to resize image.')


def define_aux_disk(kanodconfig, disk, i):
    '''Adds auxiliary disks

    If the disk name begins with an integer, assume it is an integer and
    allocate a disk of this size. If it ends with a known suffix, assumes it
    is a path and allocate it according to this path. Otherwise assumes it is a
    volume. Use libvirt syntax in that case (/ to separate group from name).
    '''

    if not(isinstance(disk, str)) or len(disk) < 2:
        raise Exception(f'Bad specification for disk: {disk}')
    elif disk[0].isdigit():
        diskname= path.join(kanodconfig, f'disk-{i}.raw')
        command = ['qemu-img', 'create', '-f', 'raw', diskname, disk]
        subprocess.run(command, check=True)
        return ['--disk', f'device=disk,path={diskname},bus=virtio,format=raw']
    elif disk.endswith('.raw'):
        return ['--disk', f'device=disk,path={disk},bus=virtio,format=raw']
    elif disk.endswith('.qcow2') or disk.endswith('.img'):
        return ['--disk', f'device=disk,path={disk},bus=virtio,format=qcow2']
    else:
        return ['--disk', f'device=disk,vol={disk},bus=virtio,format=raw']


def create_vm(kanodconfig, config, has_disk, has_cloud_init):
    vm = config.get('vm', {})
    ram = vm.get('ram', 8192)
    vcpus = vm.get('vcpus', 4)
    imgFormat = config.get('imageFormat', 'qcow2')
    command = [
        'virt-install', '--print-xml', '1', '--name', config.get('name'),
        '--noautoconsole', '--ram', f'{ram}', '--vcpus', f'{vcpus}',
        '--cpu', 'host-passthrough', '--os-variant', 'generic']
    if has_disk:
        command += [
            '--disk',
            (f'path={kanodconfig}/disk.{imgFormat},'
             f'bus=virtio,format={imgFormat}')]
    pxe = vm.get('pxe', False)
    if pxe:
        command += ['--pxe']
    uefi = vm.get('uefi', False)
    if uefi:
        command += ['--boot', 'uefi']
    cpuset = vm.get('cpuset', None)
    if cpuset is not None:
        command += [ '--cpuset', cpuset]
    if has_cloud_init:
        command += ['--disk', f'{kanodconfig}/cidata.iso,device=cdrom']
    disks = vm.get('disks', [])
    for i, disk in enumerate(disks):
        command += define_aux_disk(kanodconfig, disk, i)
    use_tpm = vm.get('tpm', False)
    if use_tpm:
        command += [
            '--tpm',
            'backend.type=emulator,backend.version=2.0,model=tpm-tis']

    domain = f'{kanodconfig}/domain.xml'
    with open(domain, 'w', encoding='utf-8') as fd:
        subprocess.run(command, check=True, stdout=fd)

    direct = vm.get('networks', None)
    interfaces.define_interfaces_xml(domain, direct)

    if subprocess.run(['virsh', 'define', domain], check=True).returncode != 0:
        raise Exception(f'Defining domain failed: {domain}')

    if subprocess.run(['virsh', 'start', config.get('name')], check=True).returncode != 0:
        raise Exception(f'Starting VM failed: {config.get("name")}')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config",
        help="Specify an alternate config folder"
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="Quiet mode")
    parser.add_argument(
        "-i", "--img",
        help="Image name to use"
    )
    parser.add_argument(
        "-v", "--validate", action="store_true",
        help="Validate the configuration"
    )
    parser.add_argument(
        "-V", "--validation-file",
        help="Validate the configuration"
    )
    args = parser.parse_args()
    config_util.setup_yaml_formatter()
    config_util.compliant_yaml_1_2()
    config_util.check_command([
        'genisoimage', 'qemu-img', 'ssh-keygen',
        'virsh', 'virt-install'])

    kanodconfig, config = common.find_config(args)
    common.vault_prepare(config)
    maven_context = config_util.maven_context(config, kanodconfig)
    if args.validate:
        validator.validate(
            kanodconfig, config, maven_context,
            file=args.validation_file, aux=validator.VM_SCHEMA)
    common.get_keys(kanodconfig, config)

    name = config.setdefault('name', 'test')
    cleanup_previous_vm(name)
    vm = config.get('vm', {})
    has_cloud_init = vm.get('cloud-init', True)
    if has_cloud_init:
        common.make_cloud_iso(kanodconfig, config, maven_context, burner=False)
    target_disk = path.join(kanodconfig, 'disk.qcow2')
    has_disk = common.define_disk(
        kanodconfig, config, maven_context, target_disk, args.img)
    if has_disk:
        resize_disk(config, target_disk)
    disk_script = vm.get('post-config', None)
    if disk_script is not None:
        command = [disk_script, kanodconfig]
        print(f'applying post configuration: {disk_script}')
        subprocess.run(command, check=True)
    create_vm(kanodconfig, config, has_disk, has_cloud_init)
    is_stopped = vm.get('stopped', False)
    if is_stopped:
        command = ['virsh', 'destroy', name]
        time.sleep(1)
        subprocess.run(command, check=True)
