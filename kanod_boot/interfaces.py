#  Copyright (C) 2021-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import os
import xml.etree.ElementTree as ET


def define_interfaces_xml(domain, itfs, model='virtio'):
    '''Adds interface definitions to a domain XML file'''
    base_udp = os.environ.get('KANOD_UDP_BASE', 22000)
    tree = ET.parse(domain)
    root = tree.getroot()
    devices = root.find('devices')
    # Remove interface introduced by virt-install
    for itf in devices.findall('interface'):
        devices.remove(itf)

    # Add all interfaces at the end of domain

    if itfs is None:
        attrs = {'type': 'network'}
        itf_element = ET.SubElement(devices, 'interface', attrs)
        ET.SubElement(itf_element, 'source', {'network': 'default'})
        ET.SubElement(itf_element, 'model', {'type': 'virtio'})
    else:
        for itf in itfs:
            interface = itf.get('interface', None)
            bridge = itf.get('bridge', None)
            network = itf.get('network', None)
            wire = itf.get('wire', None)
            mac = itf.get('mac', None)
            hwcs = itf.get('hwcs', True)
            if interface is not None:
                attrs = {'type': 'direct'}
                itf_element = ET.SubElement(devices, 'interface', attrs)
                ET.SubElement(
                    itf_element, 'source',
                    {'dev': interface, 'mode': 'bridge'})
            elif bridge is not None:
                attrs = {'type': 'bridge'}
                itf_element = ET.SubElement(devices, 'interface', attrs)
                ET.SubElement(itf_element, 'source', {'bridge': bridge})
            elif network is not None:
                attrs = {'type': 'network'}
                itf_element = ET.SubElement(devices, 'interface', attrs)
                ET.SubElement(itf_element, 'source', {'network': network})
            elif wire is not None:
                if not (type(wire) == int):
                    raise Exception(
                        f'wires must be integers not {type(wire)}')
                attrs = {'type': 'udp'}
                wire = wire * 2
                if wire < 0:
                    (src, dst) = (base_udp - wire + 1, base_udp - wire)
                else:
                    (src, dst) = (base_udp + wire, base_udp + wire + 1)
                if src < 1000 or src > 65534:
                    raise Exception(f'wire range: 1000-65534 ({wire})')
                itf_element = ET.SubElement(devices, 'interface', attrs)
                source_elt = ET.SubElement(
                    itf_element, 'source',
                    {'address': '127.0.0.1', 'port': str(src)})
                ET.SubElement(
                    source_elt, 'local',
                    {'address': '127.0.0.1', 'port': str(dst)})
            else:
                continue

            ET.SubElement(itf_element, 'model', {'type': model})
            if not hwcs:
                driver_elt = ET.SubElement(itf_element, 'driver', {'name':'vhost'})
                ET.SubElement(driver_elt, 'guest', {'csum': 'off',  'tso4': 'off', 'tso6': 'off', 'ecn': 'off', 'ufo': 'off'})

            if mac is not None:
                ET.SubElement(itf_element, 'mac', {'address': mac})
            filter = itf.get('filter', None)
            if filter is not None:
                ET.SubElement(itf_element, 'filterref', {'filter': filter})

    tree.write(domain, encoding='utf-8')
