#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import copy
import jsonschema
from os import path
import pkg_resources
import sys
import typing
import yaml
import requests

from . import common


def load_yaml_resource(name):
    return yaml.safe_load(pkg_resources.resource_string(__name__, name))


def is_object(schema):
    return schema.get('type', None) == 'object'


def augment_schema(schema, aux):
    if (not is_object(schema) or not is_object(aux)):
        raise Exception('Non mergeable schemas')
    schema.setdefault('properties', {}).update(aux.get('properties', {}))
    schema.setdefault('definitions', {}).update(aux.get('definitions', {}))
    schema.setdefault('required', []).extend(aux.get('required', []))


COMMON_SCHEMA = load_yaml_resource('common_schema.yaml')
VM_DIFF_SCHEMA = load_yaml_resource('vm_schema.yaml')
ISO_DIFF_SCHEMA = load_yaml_resource('iso_schema.yaml')

VM_SCHEMA = typing.cast(typing.Any, copy.deepcopy(COMMON_SCHEMA))
ISO_SCHEMA = typing.cast(typing.Any, copy.deepcopy(COMMON_SCHEMA))

augment_schema(VM_SCHEMA, VM_DIFF_SCHEMA)
augment_schema(ISO_SCHEMA, ISO_DIFF_SCHEMA)


def find_schema(kanodconfig, config, maven_config, file=None):
    if file is None:
        schema_file = path.join(kanodconfig, 'schema.yaml')
    else:
        schema_file = file
    if path.exists(schema_file):
        with open(schema_file, 'r') as fd:
            schema = yaml.safe_load(fd)
    else:
        if maven_config is None:
            return
        (_, verify) = maven_config
        img = config.get('image', None)
        if img is None:
            os_name = config.get('os', 'bionic')
            kubernetes = config.get('kubernetes', {})
            k8s = kubernetes.get('version', 'v1.18.8')
            img = f'{os_name}-{k8s}'
        version = config.get('version', 'latest')
        url = common.get_maven_url(maven_config, img, version, 'yaml', 'schema')
        with requests.get(url, verify=verify, stream=True) as r:
            r.raise_for_status()
            schema = yaml.safe_load(r.text)
    return schema


def validate(kanodconfig, config, maven_config, file=None, aux=None):
    schema = find_schema(kanodconfig, config, maven_config, file)
    if schema is None:
        print('Cannot find the validation json schema')
        return
    if aux is not None:
        augment_schema(schema, aux)
    errors = 0
    validator = jsonschema.Draft7Validator(
        schema,
        format_checker=jsonschema.draft7_format_checker)
    for error in validator.iter_errors(config):
        errors += 1
        msg = error.message.replace('\n', '\n  ')
        path = '.'.join([str(e) for e in error.absolute_path])
        print(f'* {msg}')
        print(f'  at {"." if path == "" else path}')
    if errors > 0:
        print()
        print(f'Found {errors} errors.')
        sys.exit(1)
    else:
        print('validation succeeded')
